	function init(){
		summary();
		errors();
		tabsTopProducts();		
		
		// Load the Visualization API and the piechart package.
		google.charts.load('visualization', '1.0', {'packages':['corechart','line','table']});

		// Set a callback to run when the Google Visualization API is loaded.
		google.charts.setOnLoadCallback(drawCharts);				
	}
	
	function readTextFile(file){
		var rawFile = new XMLHttpRequest();
		var allText;
		rawFile.open("GET", file, false);		
		rawFile.onreadystatechange = function ()
		{
			if(rawFile.readyState === 4)
			{
				if(rawFile.status === 200 || rawFile.status == 0)
				{
					allText = rawFile.responseText;
				}
			}
		}
		rawFile.send(null);
		
		return allText;
	}
	
	function parseArray(file, removeColumns){
		var parseInfo = JSON.parse(readTextFile(file));

		for(var i = 0; i < parseInfo.length; i++){
			if(i == 0 && removeColumns == true){
				parseInfo.splice(i, 1);
			}
		}
		
		return parseInfo;
	}		
	
	function summary(){	
		$.getJSON("sales_summary.json", function(result){
            $.each(result, function(i, field){
				if(i == "faturamento"){
					$(".faturamento").html("R$ " + field);
				}else if(i == "pedidos"){
					$(".pedidos").html(field);	
				}else if(i == "ticket_medio"){
					$(".ticket_medio").html("R$ " + field);	
				}else if(i == "ticket_medio_final"){
					$(".ticket_medio_final").html("R$ " + field);	
				}                
            });
        });
	}
	
	function errors(){	
		$.getJSON("errors.json", function(result){
            $.each(result, function(i, field){
				if(i == "product"){
					$(".product").html(field);
				}else if(i == "order"){
					$(".order").html(field);	
				}               
            });
        });
	}	
	
	function tabsTopProducts(){
		$.getJSON("top_products.json", function(result){
            $.each(result.marketplaces, function(i, field){
				$(".ph-title-links").append("<a href='javascript:topProducts(\"" + field + "\")' class='maisvendidos-link' id='" + field + "'>" + field + "</a>")
            });
        });	
	}
	
	function topProducts(selected){	
		var arrayTopProducts = "";
		
		$.ajaxSetup({
			async: false
		});		
	
		$.getJSON("top_products.json", function(result){			
			$.each(result.products, function(iTP, fieldTP){			
				if(selected == iTP){
					var i = 0;
					
					$.each(result.products[selected], function(iP, product){
						if(product != null){
							arrayTopProducts += "['" + product.name + "', " + product.qtd_sold + ", " + product.total_money + "],";
						}
					});
				}
			});
        });
		
		return JSON.parse("[" + arrayTopProducts.slice(0, -1) + "]");
	}	
	
	function drawCharts() {		
		//Historico por preco
		var dataHistorico = google.visualization.arrayToDataTable([
			['Data', 'Magento', 'Extra', 'Walmart', 'Mercadolivre'],
			['10/11/2016',  80,      20, 10, 15],
			['11/11/2016',  50,      10, 1, 25],
			['12/11/2016',  10,      5, 20, 45],
			['13/11/2016',  30,      40, 85, 65]
		]);

		var formatter = new google.visualization.NumberFormat({
			prefix: 'R$'
		});			
		
		formatter.format(dataHistorico,1);	
		formatter.format(dataHistorico,2);	
		
		var optionsHistorico = {
			legend: { position: 'bottom' },
			width: '100%',  
			pointSize: 5,
			vAxis: {  format:'R$#######' },
			hAxis: {  slantedText:true, slantedTextAngle:30, height:100 },
			chartArea: {
				top: 5,
				height: '700px'
			}
        };

        var chartHistorico = new google.visualization.LineChart(document.getElementById('historicopreco'));

        chartHistorico.draw(dataHistorico, optionsHistorico);
		
		//Mais vendidos	
		var dataMasVendidos = new google.visualization.DataTable();
        dataMasVendidos.addColumn('string', 'Nome');
        dataMasVendidos.addColumn('number', 'Quantidade');
        dataMasVendidos.addColumn('number', 'Receita');
	
		//TO-DO
        dataMasVendidos.addRows([
			['Celular Galaxy S5 Mini Duos Tela 4.5”...', 15, 14842.320000000003],
			['Cadeira para Auto Cosmo SP - Princesa...', 2, 732.72],
			['Forno Elétrico Tostador 110V - Black&...', 4, 670.6],
			['Music Box Bluetooth - Multilaser', 2, 662.62],
			['Tablet 7A-P111A40 Android 4.0, 8GB Pr...', 2, 539.8]
        ]);
		
		$(".maisvendidos-link").click(function(){
			$(".maisvendidos-link").removeClass("active");
			$(this).addClass("active");
		});

        var table = new google.visualization.Table(document.getElementById('maisvendidos'));

        table.draw(dataMasVendidos, { width: '100%', height: '100%', allowHtml: true});			
		
		//Vendas por regiao
		var dataRegiao = new google.visualization.DataTable();
        dataRegiao.addColumn('string', 'Estado');
        dataRegiao.addColumn('string', 'Cidade');
        dataRegiao.addColumn('number', 'Vendas');
        dataRegiao.addColumn('number', 'Valor total');	
        dataRegiao.addRows(parseArray("sales_geochart.json",true));

        var table = new google.visualization.Table(document.getElementById('vendasregiao'));

        table.draw(dataRegiao, { width: '100%', height: '100%', allowHtml: true});		
		
		//Pedidos
		var data = google.visualization.arrayToDataTable(parseArray("breakdown_chart.json",false));

		var options = {
			width: '100%',
			height: '100%',	  
			hAxis: {
			  minValue: 0, textStyle:{fontSize:'8px', bold: true}
			},
			legend:{position:'none'},
			isStacked: true,
			chartArea: {
				top: 5,
				height: '90%'
			}
		};

		var chart = new google.visualization.BarChart(document.getElementById('pedidos'));
		chart.draw(data, options);

		function resizeHandler () {
			chartHistorico.draw(dataHistorico, optionsHistorico);
			table.draw(dataRegiao, { width: '100%', height: '100%', allowHtml: true});		
			chart.draw(data, options);
		}
		
		if (window.addEventListener) {
			window.addEventListener('resize', resizeHandler, false);
		}
		else if (window.attachEvent) {
			window.attachEvent('onresize', resizeHandler);
		}		
	}		
	
	init();